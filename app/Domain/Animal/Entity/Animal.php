<?php
declare(strict_types=1);

namespace App\Domain\Entity\Animal;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Class Animal
 * @package App\Domain\Entity\Animal;
 */
class Animal implements Arrayable
{
    /**
     * @var null|array
     */
    private ?array $popularNames;

    /**
     * @var null|string
     */
    private ?string $specie;

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [];
    }
}
